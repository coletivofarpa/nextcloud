# Nextcloud com SSL em conteiner
| [🇪🇸](docs/README_es.md) | [🇬🇧](docs/README_en.md) |
## Instalando o Docker latest
Atualize o índice de pacotes do gerenciador <code>apt</code>
```bash
sudo apt-get update
```
Instale pacotes para permitir apt usar um repositório sobre HTTPS
```bash
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```
Adicione a chave GPG oficial do Docker:
```bash
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
```
Use o seguinte comando para configurar o repositório
```bash
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```
Conceda permissão de leitura para o arquivo de chave pública do Docker
```bash
sudo chmod a+r /etc/apt/keyrings/docker.gpg
```
Atualize o índice de pacote do <code>apt</code>
```bash
sudo apt-get update
```
Instale o Docker Engine, containerd e o Docker Compose.
```bash
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

## Alterações antes de rodar pela primeira vez

Copie o arquivo `.env.example` para `.env` e altere os valores das variáveis de ambiente.

```
cp .env.example .env
```

| Ambiente | Serviço | 
|-------------|---------|
| [`VIRTUAL_HOST`](https://github.com/nginx-proxy/nginx-proxy#usage) | `web` |
| [`LETSENCRYPT_HOST`](https://github.com/nginx-proxy/docker-letsencrypt-nginx-proxy-companion/blob/master/docs/Basic-usage.md#step-3---proxyed-containers) | `web` |
| [`LETSENCRYPT_EMAIL`](https://github.com/nginx-proxy/docker-letsencrypt-nginx-proxy-companion/blob/master/docs/Let's-Encrypt-and-ACME.md#contact-address) | `web` |
| `POSTGRES_PASSWORD` | `db` |
| `NEXTCLOUD_TRUSTED_DOMAINS` | `app` |

> **PS**: O Let's Encrypt somente funciona em servidores quando `VIRTUAL_HOST` e `LETSENCRYPT_HOST` possuirem um domínio público válido registrado em um servidor DNS. Não tente utilizar localhost, não irá funcionar!

## Crie uma rede utilizando o seguinte comando:
```bash
docker network create proxy-reverso
```

## Execução
Para o seu ambiente funcionar, utilize os seguintes comandos:
```bash
docker compose up -d
docker compose -f docker-compose.proxy.yml up -d
```

## Após a instalação
Após terminado, abra a seguinte url https://SEU-DOMINIO/settings/admin/overview


Caso seja necessário rodar algum comando `occ`, utilize os seguintes comandos:

```bash
docker compose exec -u www-data app ./occ db:add-missing-indices
docker compose exec -u www-data app ./occ db:convert-filecache-bigint
```
> **OBS**: app é o nome do seu container. Para listar os containers, utilize o comando ```docker-compose ps```

## Configurações personalizadas do PHP

Caso você precisar alterar alguma configuração do PHP, acesse o seguinte arquivo [`.docker/app/config/php.ini`](/.docker/app/config/php.ini).


## Utilizando uma versão específica do Nextcloud

Altere o  [Dockerfile](/.docker/app/Dockerfile#L1) na linha de número 1 e coloque a versão desejada.

Construa as imagens e levante o container novamente:

```bash
docker compose build
docker compose down
docker compose up -d
```

Caso quiser ver as alterações, rode:
```bash
docker compose logs -ft
```
Você verá a seguinte mensagem nos logs, além de outras várias mensagens de upgrade:

```
app_1      | 2020-04-28T19:49:38.568623133Z Initializing nextcloud 18.0.4.2 ...
app_1      | 2020-04-28T19:49:38.577733913Z Upgrading nextcloud from 18.0.3.0 ...
```
